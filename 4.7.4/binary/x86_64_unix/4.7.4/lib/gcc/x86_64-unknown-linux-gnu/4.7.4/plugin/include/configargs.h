/* Generated automatically. */
static const char configuration_arguments[] = "../gcc-4.7.4/configure --prefix=/Scr/scr-test-steven/install/gcc/4.7.4 --disable-multilib --with-gmp=/Scr/scr-test-steven/install/libgmp/with_cxx_support/for_gcc_4.4.7/4.3.2 --with-mpfr=/Scr/scr-test-steven/install/libmpfr/2.4.2 --with-mpc=/Scr/scr-test-steven/install/libmpc/0.8.1 --with-cloog=/Scr/scr-test-steven/install/libcloog/0.15.11 --with-ppl=/Scr/scr-test-steven/install/libppl/0.11";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
